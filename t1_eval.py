#! /usr/bin/env python3

import sys, glob, argparse, subprocess, socket

os.environ['FSLOUTPUTTYPE'] = 'NIFTI_GZ'

# Last modified
last_modified = "Created by Anders Perrone 4/20/2017. Last modified by Anders Perrone 4/24/2017"

# Program description
prog_descrip =  """%(prog)s: T1_matcher returns eta squared values for each T1 to the study template T1."""


# Get path to the FSL directory, compiled Matlab eta squared function, and Matlab Runtime Environment
try:
    if socket.gethostname() == 'rushmore':
        FSL_DIR = '/usr/share/fsl/5.0/bin/'
        ETA_DIR = '/mnt/max/shared/utilities/compiled_Matlab_code/eta_squared/for_testing'
        MRE = '/mnt/max/shared/code/external/utilities/Matlab2016bRuntime/v91'
    elif 'exa' in socket.gethostname():
        FSL_DIR = '/opt/installed/fsl-5.0.0/bin/'
        ETA_DIR = '/mnt/lustre1/fnl_lab/code/internal/utilities/SEFM_eval'
        MRE = '/mnt/lustre1/fnl_lab/code/external/utilities/Matlab2016bRuntime/v91'
    else:
        FSL_DIR = '' 
        ETA_DIR = os.path.dirname(os.path.realpath(sys.argc[0]))
        MRE = os.path.dirname(os.path.realpath(sys.argv[0])) + "/Matlab2016bRuntime/v91"
        print('Running in Docker or some external environment')
except:
        print("Error: Host not recognized")

def list_T1s(subject_id, subject_dir):
    all_T1s = glob.glob(subject_dir + '/' + subject_id + '_T1w_MPR*.nii.gz')
    series_nums = []
    for nii in all_T1s:
        p2 = nii.split("_T1w_MPR")[1]
        series_num = p2.split(".nii.gz")[0]
        series_nums.append(int(series_num))
    return series_nums

def rigid_align(subject_id, subject_dir, series_nums, atlas, temp_dir):
    for i in series_nums:
        input = subject_dir + '/' + subject_id + '_T1w_MPR' + str(i) + '.nii.gz'
        output = temp_dir + '/' + subject_id + '_T1w_MPR' + str(i) + 'aligned2atlas.nii.gz'
        align_cmd = [FSL_DIR + 'flirt', '-in', input, '-ref', atlas, '-dof', str(6), '-out', output]
        print(align_cmd)
        subprocess.call(align_cmd)
    return

def mask(subject_id, subject_dir, series_nums, temp_dir):
    for i in series_nums:
        input = temp_dir + '/' + subject_id + '_T1w_MPR' + str(i) + 'aligned2atlas.nii.gz'
        output = temp_dir + '/' + subject_id + '_T1w_MPR' + str(i) + 'aligned2atlas_masked.nii.gz'
        mask_cmd = [FSL_DIR + 'bet', input, output]
        print(mask_cmd)
        subprocess.call(mask_cmd)
    return

def eta_squared(subject_id, subject_dir, series_nums, temp_dir, atlas):
    min_eta_dict = {}
    for i in series_nums:
        masked_t1 = temp_dir + '/' + subject_id + '_T1w_MPR' + str(i) + 'aligned2atlas_masked.nii.gz'
        eta_cmd = [ETA_DIR + '/run_eta_squared.sh', MRE, masked_t1, atlas ]
        print(eta_cmd)
        eta_stdout = subprocess.check_output(eta_cmd)
        eta_value = float(eta_stdout.split()[-1])
        min_eta_dict[i] = eta_value
    print(min_eta_dict)
    best_t1 = max(min_eta_dict, key=min_eta_dict.get)
    print(best_t1)
    return

def main(argv=sys.argv):
    arg_parser = argparse.ArgumentParser(description=prog_descrip)
    arg_parser.add_argument('-s', '--subject-id', metavar='SUBJECT_ID', action='store', required=True,
                            help=('The subject id'),
                            dest='subject_id')
    arg_parser.add_argument('-d', '--subject-dir', metavar='SUBJECT_DIR', action='store', required=True,
                            help=('Full path to the subject directory containing SEFM niftis'),
                            dest='subject_dir')
    arg_parser.add_argument('-a', '--atlas', metavar='ATLAS', action='store', required=True,
                            help=('Study specific atlas created by ANTs'),
                            dest='atlas')
    args = arg_parser.parse_args()

    # Make a temporary working directory
    temp_dir = args.subject_dir + '/T1_eta_temp'
    print(args.subject_id)
    print(temp_dir)
    print(['mkdir', temp_dir])
    subprocess.call(['mkdir', temp_dir])

    # Get The series num of each T1 image
    series_nums = list_T1s(args.subject_id, args.subject_dir)

    # Align each brain to the template
    rigid_align(args.subject_id, args.subject_dir, series_nums, args.atlas, temp_dir)

    # Mask each subject anatomical image
    mask(args.subject_id, args.subject_dir, series_nums, temp_dir)

    # Calculate the eta squared value for each anatomical image to the atlas
    eta_squared(args.subject_id, args.subject_dir, series_nums, temp_dir, args.atlas)


if __name__ == "__main__":
    sys.exit(main())
