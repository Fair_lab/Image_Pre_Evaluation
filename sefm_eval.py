#!/home/exacloud/lustre1/fnl_lab/code/external/utilities/exahead1-anaconda3/bin/python3

import os, sys, glob, argparse, subprocess, socket, operator

os.environ['FSLOUTPUTTYPE'] = 'NIFTI_GZ'

# Last modified
last_modified = "Created by Anders Perrone 3/21/2017. Last modified by Anders Perrone 5/5/2017"

# Program description
prog_descrip =  """%(prog)s: sefm_eval pairs each of the pos/neg sefm and returns the pair that is most representative
                   of the average by calculating the eta squared value for each sefm pair to the average sefm.""" + last_modified

# Get path to the FSL directory, compiled Matlab eta squared function, and Matlab Runtime Environment
try:
    if socket.gethostname() == 'rushmore':
        FSL_DIR = '/usr/share/fsl/5.0/bin/'
        ETA_DIR = '/mnt/max/shared/utilities/compiled_Matlab_code'
        MRE = '/mnt/max/shared/code/external/utilities/Matlab2016bRuntime/v91'
    elif 'exa' in socket.gethostname():
        FSL_DIR = '/opt/installed/fsl-5.0.10/bin/'
        ETA_DIR = '/home/exacloud/lustre1/fnl_lab/code/internal/utilities/SEFM_eval'
        MRE = '/home/exacloud/lustre1/fnl_lab/code/external/utilities/Matlab2016bRuntime/v91'
        #subprocess.call('. ' + FSL_DIR + '../etc/fslconf/fsl.sh',shell=True)
    elif socket.gethostname() == 'beast':
        FSL_DIR = '/usr/lib/fsl/5.0/'
        print('This script has not yet been configured to run on beast, sorry.')
except:
        print("Error: Host not recognized")

def pair(subject_ID, subject_dir):
    #
    list_pos = glob.glob(subject_dir + '/' + subject_ID + '_SpinEchoPhaseEncodePositive*.nii.gz')
    list_neg = glob.glob(subject_dir + '/' + subject_ID + '_SpinEchoPhaseEncodeNegative*.nii.gz')

    try:
        len(list_pos) == len(list_neg)
    except:
        print("Error: There are a mismatched number of SEFMs")
    # Create a sorted list of the SE Positive series numbers
    pos_nums = []
    for nii in list_pos:
        p2 = nii.split("Positive")[1]
        series_num = p2.split(".nii.gz")[0]
        try:
            int(series_num)
        except ValueError:
            print("Error: series number not recognized, make sure all SEFM have valid series numbers")
            quit()
        pos_nums.append(int(series_num))
    pos_nums = sorted(pos_nums)
    # Create a sorted list of the SE Negative series numbers
    neg_nums = []
    for nii in list_neg:
        p2 = nii.split("Negative")[1]
        series_num = p2.split(".nii.gz")[0]
        neg_nums.append(int(series_num))
    neg_nums = sorted(neg_nums)
    print("positive series numbers found:")
    print(pos_nums)
    print("negative series numbers found:")
    print(neg_nums)
    # Create a list of the pairings. Always in the form (neg,pos)
    pairs = []
    if pos_nums[0] < neg_nums[0]:
        for i in pos_nums:
            if i + 1 in neg_nums:
                if len(pairs):
                    for pair in pairs:
                        if i + 1 in pair:
                            break
                        else:
                            pairs.append((i+1, i))
                            break
                else:
                    pairs.append((i+1, i))
                    continue
            elif i - 1 in neg_nums:
                if len(pairs):
                    for pair in pairs:
                        if i - 1 in pair:
                            break
                        else:
                            pairs.append((i-1, i))
                            break
                else:
                    pairs.append((i-1, i))
                    continue
            else:
                print("Warning: " + subject_ID + "_SpinEchoPhaseEncodePositive" + str(i) + ".nii.gz  missing negative SE pair")
                continue
    elif neg_nums[0] < pos_nums[0]:
        for j in neg_nums:
            if j + 1 in pos_nums:
                if len(pairs):
                    for pair in pairs:
                        if j + 1 in pair:
                            break
                        else:
                            pairs.append((j, j+1))
                            break
                else:
                    pairs.append((j, j + 1))
                    continue
            elif j - 1 in pos_nums:
                if len(pairs):
                    for pair in pairs:
                        if j - 1 in pair:
                            break
                        else:
                            pairs.append((j, j-1))
                            break
                else:
                    pairs.append((j, i - 1))
                    continue
            else:
                print("Warning: " + subject_ID + "_SpinEchoPhaseEncodeNegative" + str(j) + ".nii.gz missing positve SE pair")
                continue
    return pairs

def rigid_align(subject_id, subject_dir, series_pairs, temp_dir):
    # Do a rigid body alignment with all the pos/neg SEFMs
    for i in series_pairs:
        pos_ref = subject_dir + '/' + subject_id + '_SpinEchoPhaseEncodePositive' + str(series_pairs[0][1]) + '.nii.gz'
        neg_ref = subject_dir + '/' + subject_id + '_SpinEchoPhaseEncodeNegative' + str(series_pairs[0][0]) + '.nii.gz'
        pos_input = subject_dir + '/' + subject_id + '_SpinEchoPhaseEncodePositive' + str(i[1]) + '.nii.gz'
        neg_input = subject_dir + '/' + subject_id + '_SpinEchoPhaseEncodeNegative' + str(i[0]) + '.nii.gz'
        pos_out = temp_dir + '/init_pos_reg' + str(i[1]) + '.nii.gz'
        neg_out = temp_dir + '/init_neg_reg' + str(i[0]) + '.nii.gz'
        pos_cmd = [FSL_DIR + 'flirt', '-in', pos_input, '-ref', pos_ref, '-dof', str(6), '-out', pos_out]
        neg_cmd = [FSL_DIR + 'flirt', '-in', neg_input, '-ref', neg_ref, '-dof', str(6), '-out', neg_out]
        subprocess.call(pos_cmd, stdout=subprocess.DEVNULL)
        subprocess.call(neg_cmd, stdout=subprocess.DEVNULL)
    return

def aligned_average(series_pairs, temp_dir):
    # Average the pos/neg SEFMs after alignment
    
    # First sum all of the images together
    pos_sum_cmd = [FSL_DIR + 'fslmaths', temp_dir + '/init_pos_reg' + str(series_pairs[0][1]) + '.nii.gz']
    neg_sum_cmd = [FSL_DIR + 'fslmaths', temp_dir + '/init_neg_reg' + str(series_pairs[0][0]) + '.nii.gz']
    for i in series_pairs[1:]:
        pos_sum_cmd += ['-add', temp_dir + '/init_pos_reg' + str(i[1]) + '.nii.gz']
        neg_sum_cmd += ['-add', temp_dir + '/init_neg_reg' + str(i[0]) + '.nii.gz']
    pos_sum_cmd += [temp_dir + '/pos_sum.nii.gz']
    neg_sum_cmd += [temp_dir + '/neg_sum.nii.gz']
    subprocess.call(pos_sum_cmd)
    subprocess.call(neg_sum_cmd)

    # Divide the sum by the number of pos/neg SEFMs to get the average
    num_sefm = len(series_pairs)
    pos_avg_cmd = [FSL_DIR + 'fslmaths', temp_dir + '/pos_sum.nii.gz', '-div', str(num_sefm), temp_dir + '/pos_mean.nii.gz']
    neg_avg_cmd = [FSL_DIR + 'fslmaths', temp_dir + '/neg_sum.nii.gz', '-div', str(num_sefm), temp_dir + '/neg_mean.nii.gz']
    subprocess.call(pos_avg_cmd)
    subprocess.call(neg_avg_cmd)
    return

def eta_squared(series_pairs, temp_dir, subject_id, subject_dir):
    # Calculate the eta squared value of each aligned image to the average and return the pair with the highest average
    #avg_eta_dict = {}
    min_eta_dict = {}
    for i in series_pairs:
        pos_mat_cmd = [ETA_DIR + '/run_eta_squared.sh', MRE, temp_dir + '/init_pos_reg' + str(i[1]) + '.nii.gz', temp_dir + '/pos_mean.nii.gz']
        neg_mat_cmd = [ETA_DIR + '/run_eta_squared.sh', MRE, temp_dir + '/init_neg_reg' + str(i[0]) + '.nii.gz', temp_dir + '/neg_mean.nii.gz']
        #print(pos_mat_cmd)
        #print(neg_mat_cmd)
        pos_mat_stdout = subprocess.check_output(pos_mat_cmd)
        neg_mat_stdout = subprocess.check_output(neg_mat_cmd)
        pos_eta = float(pos_mat_stdout.split()[-1])
        print(subject_id + "_SpinEchoPhaseEncodePositive" + str(i[1]) + ".nii.gz eta value = " + str(pos_eta))
        neg_eta = float(neg_mat_stdout.split()[-1])
        print(subject_id + "_SpinEchoPhaseEncodeNegative" + str(i[0]) + ".nii.gz eta value = " + str(neg_eta))
        # instead of finding the average between eta values between pairs. Take the pair with the highest lowest eta value.
        min_eta = min([pos_eta, neg_eta])
        min_eta_dict[i] = min_eta
        #avg_eta = (pos_eta + neg_eta)/2
        #avg_eta_dict[i] = avg_eta
    #print(avg_eta_dict)
    #print(max(avg_eta_dict, key=avg_eta_dict.get))
    best_pair = max(min_eta_dict, key=min_eta_dict.get)
    best_pos = subject_dir + "/" + subject_id + "_SpinEchoPhaseEncodePositive" + str(best_pair[1]) + ".nii.gz"
    link_pos = subject_dir + "/" + subject_id + "_SpinEchoPhaseEncodePositive.nii.gz"
    best_neg = subject_dir + "/" + subject_id + "_SpinEchoPhaseEncodeNegative" + str(best_pair[0]) + ".nii.gz"
    link_neg = subject_dir + "/" + subject_id + "_SpinEchoPhaseEncodeNegative.nii.gz"
    ln_pos_cmd = ['ln', '-sf', best_pos, link_pos]
    ln_neg_cmd = ['ln', '-sf', best_neg, link_neg]
    subprocess.call(ln_pos_cmd)
    subprocess.call(ln_neg_cmd)
    return

def main(argv=sys.argv):
    arg_parser = argparse.ArgumentParser(description=prog_descrip)
    arg_parser.add_argument('-s', '--subject-id', metavar='SUBJECT_ID', action='store', required=True,
                             help=('The subject ID (part preceeding _SpinEchoPhase...)'),
                             dest='subject_id')
    arg_parser.add_argument('-d', '--subject-dir', metavar='SUBJECT_DIR', action='store', required=True,
                             help=('Full path to the subject directory containing niftis'),
                             dest='subject_dir')
    
    arg_parser.add_argument('-v', '--version', action='version', version='%(prog)s: ' + last_modified,
                             help=("Return script's last modifiied date"),
                             dest='subject_dir')
    args = arg_parser.parse_args()

    # Make a temporary working directory
    temp_dir = args.subject_dir + '/eta_temp'
    subprocess.call(['mkdir', temp_dir])

    # Return a list of each SEFM pos/neg pair
    series_pairs = pair(args.subject_id, args.subject_dir)
    print("Pairing:")
    print(series_pairs)

    # Create rigid body alignments
    print("Aligning SEFMs and creating template")
    rigid_align(args.subject_id, args.subject_dir, series_pairs, temp_dir)
    aligned_average(series_pairs, temp_dir)
    print("Computing ETA squared value for each image to the template")
    eta_squared(series_pairs, temp_dir, args.subject_id, args.subject_dir)

    # Delete the temp directory containing all the intermediate images
    rm_cmd = ['rm', '-rf', temp_dir]
    subprocess.call(rm_cmd)

    print("Success! Best SEFM pair has been chosen and linked in the subject's nifti directory.")


if __name__ == "__main__":
    sys.exit(main())






